#include<stdio.h>
int main()
{
    int original,reversed=0,n,r;
    printf("Enter an integer\n");
    scanf("%d",&n);
    original=n;
    while(n!=0)
    {
        r=n%10;
        reversed=reversed*10 + r;
        n=n/10;
    }
    
    if(original==reversed)
    {
        printf("The number is a palindrome\n");
        
    }
    else
    {
        printf("The number is not a palindrome\n");
    }
    return 0;
}