#include <stdio.h>

struct employee
{
    char name[30];
    int id;
    float salary;
    char DOJ[10];

};

int main()
{
    struct employee e;
    printf("Enter name of the employee: ");
    scanf("%s",e.name);
    printf("Enter ID of employee: ");
    scanf("%d",&e.id);
    printf("Enter salary of employee: ");
    scanf("%f",&e.salary);
    printf("Enter date of join of the employee: ");
    scanf("%s",e.DOJ);
    printf("The credentials of the employee are: \n");
    printf("\n Name: %s",e.name);
    printf("\n ID: %d",e.id);
    printf("\n Salary: %.2f",e.salary);
    printf("\n Date of Join: %s",e.DOJ);
    return 0;
}





