#include <stdio.h>
int main()
{
    int a,b,temp;
    printf(" Enter two numbers:\n");
    scanf("%d %d", &a, &b);
    printf("Initial values of a is %d and b is %d\n"), a,b);
    temp=a;
    a=b;
    b=temp;
    printf("The new value of a is %d and b is %d\n", a,b);
    return 0;
}
